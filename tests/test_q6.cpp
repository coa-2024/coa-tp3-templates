#include "catch.hpp"

#include <vector>
#include <map>
#include <algorithm>
#include "set_functions.hpp"
#include "myclass.hpp"

using namespace std;

bool my_compare(const std::pair<int, string> &x, int y)
{
    if (x.first == y) return true;
    else return false;
}


TEST_CASE("intersection map - vector", "[Q6]")
{
    map<int, string> a, r;
    vector<int> v = {1, 3, 5};
    map<int, string> oracle = {{1, "A"}, {3, "C"}};
    
    a[1] = "A"; a[2] = "B"; a[3] = "C"; a[4] = "D";
    
    set_intersection_t(begin(a), end(a), begin(v), end(v), 
                       inserter(r, r.end()), my_compare); 

    REQUIRE(r == oracle);
}
